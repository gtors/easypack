# coding: utf-8

u""" 

EeasyPack
=========

Удобный API для взаимодействия с паками M3.

.. note::
    Использование функционала возможно только в том случае, если
    предварительно был вызван метод :meth:`ControllerCache.populate`.

Принцип работы
--------------

Модуль является перекрытым и может вызываться как функция, в качестве
параметров которой должны передаваться или имя пака, или перечисление
родительских модулей плюс имя пака.

При первом обращении к кэшу запускается его инициализация. В процессе
инициализации происходит поиск всех ``actions`` модулей в проекте, а
в них самих пойск паков. На основе собранных данных выстраивается
словарь, где ключем является разбыитый путь до пака, на каждый уровень
вложености модулей создается новый ключ.

В случае возникновения необходимости обновления кэша существует метод
:meth:`~_EasyPack.refresh`, который повторно выстроит словарь.

Если в процессе инициализации возникла коллизия паков, то уровень
на котором возникла коллизия исключается из кэша. И обращение к
паку становится доступным только на более верхнем уровне. ::

    >>> ep('first_module.SameNamePack')
    ...
    >>> ep('second_module.SameNamePack')
    ...

Варианты использования
----------------------

Поиск пака по его имени, получение экземпляра из ``ControllerCache``: ::

    >>> ep('PersonalPack').instance
    <project.personal.actions.PersonalPack at 0x7fd65398ccd0>

Поиск пака по его модулю и имени пака: ::

    >>> ep('personal.PersonalPack').instance
    <project.personal.actions.PersonalPack at 0x7fd65398ccd0>

Поиск пака по имени пака и по вложенным модулям: ::

    >>> ep('core.personal.PersonalPack').instance
    <project.personal.actions.PersonalPack at 0x7fd65398ccd0>

Получение полного пути до пака: ::

    >>> ep('PersonalPack').path
    project.personal.actions.PersonalPack

Обращение к экшену пака: ::

    >>> ep('PersonalPack').action('registred_action')
    <recordpack.helpers.SimpleAction at 0x7fd65395d990>

Получение URL пака. ::

    >>> ep('PersonalPack').url()
    /project_base_url/personal

Получение URL экшена пака. ::

    >>> ep('PersonalPack').url('dismissal_action')
    /project_base_url/personal/registred

Проверка прав пака: ::

    >>> ep('PersonalPack').has_perm(request, PERM_VIEW)
    True

Получение URL экшена (c условием, что пак не зарегистрирован в контроллере): ::

    >>> ep('NotRegistredPack').url('action_name')
    raised AttributeError ...
    >>> ep('NotRegistredPack', silent=True).url('action_name')
    None

"""
#-------------------------------------------------------------------------------
# Imports
#-------------------------------------------------------------------------------

# Stdlib
import sys
import types
import inspect
import itertools

# 3rdparty
from m3.actions.urls import get_url
from m3.actions import ActionPack, ControllerCache

#-------------------------------------------------------------------------------
# Metadata
#-------------------------------------------------------------------------------

__author__ = 'Andrey Torsunov'
__contact__ = 'torsunov@bars-open.ru'
__docformat__ = 'restructuredtext'

__version__ = '0.1.0'
__all__ = []


#-------------------------------------------------------------------------------
# Pack proxy
#-------------------------------------------------------------------------------

class Pack(object):
    
    __slots__ = ('module', 'pack')

    def __init__(self, module, pack):
        self.module = module
        self.pack = pack

    def __str__(self):
        return self.module + '.' + self.pack

    def __unicode__(self):
        return unicode(str(self))

    def __repr__(self):
        return unicode(self)

    def action(self, action_name):
        return getattr(self.instance, action_name)

    def has_perm(self, request, perm):
        self.instance.has_perm(request, perm)

    def url(self, action_name=''):
        if action_name:
            return get_url(self.action(action_name))
        else:
            return self.instance.get_absolute_url()

    @property
    def path(self):
        return str(self)

    @property
    def instance(self):
        return ControllerCache.find_pack(str(self))


class SilentPack(object):

    def __init__(self, pack):
        self.pack = pack

    def __getattr__(self, attr):
        pack_attr = getattr(self.pack, attr)
        if callable(pack_attr):
            return self._exception_killer(pack_attr)
        else:
            return pack_attr

    def _exception_killer(self, brawler):
        def killer(*args, **kwargs):
            try:
                return brawler(*args, **kwargs)
            except AttributeError:
                return None
        
  
#-------------------------------------------------------------------------------
# EasyPack proxy fabric
#-------------------------------------------------------------------------------

class _EasyPack(types.ModuleType):

    def __init__(self, *args, **kwargs):

        #: Внутренний кэш путей импорта паков.
        #: Кэш для ``plugin.package.actions.PackClass`` имеет следующий вид::
        #:
        #:     {
        #:         'package.module.PackClass': Pack,
        #:         'module.PackClass': Pack,
        #:         'PackClass': Pack,
        #:     }
        self._packs = {}

        #: Обнаруженные коллизии в процессе инициализации: ::
        #:
        #:     {
        #:         'CollisionPack': [Pack, Pack],
        #:         'users.UserCollisionPack': [Pack, Pack, ...]
        #:     }
        self._collisions = {}

        #: Ссылка на самого себя, чтобы инспектор кода не ругался при
        #: импортах.
        self.ep = self

        super(_EasyPack, self).__init__(*args, **kwargs)

    def __call__(self, lookup_key, silent=False):
        u""" Поиск пака по ключу поиска.

        Примеры возможных значений `lookup_key`:
        - 'PackName'
        - 'pack_module.PackName'
        - 'package.pack_module.PackName'
        - и т.д.

        :param lookup_key: ключ поиска
        :type lookup_key: :class:`str`

        :param silent: режим блокировки исключений
        :type silent: :class:`bool`

        :rtype: :class:`Pack`

        :raises RuntimeError: если пак не существует или
            имеют место коллизии.

        """
        # Внутренний кэш инициализируется при первом обращении
        self._packs or self._discover_packs()

        pack_proxy = self._packs.get(lookup_key)
        collision_packs = self._collisions.get(lookup_key)
        
        if silent and pack_proxy:
            return SilentPack(pack_proxy)

        if pack_proxy:
            return pack_proxy

        # TODO: выделить в отдельные исключения
        elif collision_packs:
            raise RuntimeError((
                'Packs collision in paths: {paths}'
            ).format(paths=collision_packs))
        else:
            raise RuntimeError('Unexisted pack')

    def refresh(self, populate=False):
        u""" Обновление кэша с его предварительным сбросом. 
        
        :param populate: если указано :const:`True`, то перед обновлением
            кэша будет вызван :meth:`ControllerCache.populate`

        """
        if populate:
            ControllerCache.populate()
        self._packs = {}
        self._collisions = {}
        self._discover_packs()

    def _discover_packs(self):
        u""" Поиск паков среди импортированных модулей ``actions``.

        Все найденные паки будут добавлены во внутренний кэш.

        """
        for module_name in itertools.ifilter(
            lambda m: m.endswith('.actions'),
            sys.modules
        ):
            actions_module = sys.modules[module_name]
            for module_item in dir(sys.modules[module_name]):
                pack_cls = getattr(actions_module, module_item)
                if (
                    inspect.isclass(pack_cls) and
                    issubclass(pack_cls, ActionPack) and
                    pack_cls.__module__ == module_name
                ):
                    self._add_pack(module_name, module_item)

    def _add_pack(self, module_name, pack_name):
        u""" Добавление пака в кэш.

        .. note::
            При добавлении ключа в кэш отбрасывается ``actions``.

        .. note::
            Если на каком-то уровне существуют коллизии, то этот уровень
            отбрасывается и доступ к паку возможен только на более глубоком
            уровне.

        """
        pack = Pack(module_name, pack_name)
        parts = module_name.split('.')
        parts.append(pack_name)
        parts.remove('actions')

        for offset in xrange(len(parts)):
            lookup_key = '.'.join(parts[offset:])
            if lookup_key in self._packs or lookup_key in self._collisions:
                self._add_collision(lookup_key, pack)
            else:
                self._packs[lookup_key] = pack

    def _add_collision(self, lookup_key, pack):
        u""" Добавление объектов коллизии.

        Если уровень, на котором существует коллизия присутствует
        в кэше паков, он будет незамедлительно удален.

        """
        conflict_packs = self._collisions.setdefault(lookup_key, [])
        conflict_packs.append(pack)
        pack = self._packs.pop(lookup_key, None)
        pack and conflict_packs.append(pack)

    def __dir__(self):
        u"""
        Ограничение внешних атрибутов модуля.
        """
        result = list(self.__all__)
        result.extend((
            '__file__', '__path__', '__doc__', '__all__', '__docformat__',
            '__name__', '__path__', '__package__', '__version__',
        ))
        return result


# Ссылка на старый модуль, чтобы он не был поглощен GC
old_module = sys.modules[__name__]

# Подмена модуля
ep = sys.modules[__name__] = _EasyPack(__name__)
ep.__dict__.update({
    '__file__': __file__,
    '__package__': 'easypack',
    '__doc__': __doc__,
    '__version__': __version__,
    '__all__': ['ep'],
    '__docformat__': __docformat__
})
