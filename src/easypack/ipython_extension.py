# coding: utf-8

#------------------------------------------------------------------------------
# Imports
#------------------------------------------------------------------------------

# IPython
from IPython.core.magic import Magics, magics_class, line_magic

# Own
from easypack import ep

#------------------------------------------------------------------------------
# Magics
#------------------------------------------------------------------------------

@magics_class
class EasypackMagics(Magics):
    
    @line_magic
    def pack(self, pack_name):
        if pack_name:
            self._add_variable('_pack', ep(pack_name, sillent=True).instance)

    @line_magic
    def epack(self, pack_name):
        if pack_name:
            self._add_variable('_epack', ep(pack_name))

    def _add_variable(self, name, value):
        self.shell.user_ns[name] = value


def load_ipython_extension(ip):
    ip.magics_manager.register(EasypackMagics)
