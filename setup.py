#!/usr/bin/env python

import os
import sys
sys.path.append(os.path.join(os.path.basedir(__file__), 'src'))

from setuptools import setup
import easypack

setup(
    name="easypack",
    version=easypack.__version__,
    description="Enhanced API for M3 action packs",
    long_description=open("README.rst").read(),
    author="Andrew Torsunov",
    author_email="torsunov@bars-open.ru",
    url="https://bitbucket.org/gtors/easypack",
    license="CC",
    package_data={"": ["*.py"]},
    include_package_data=True,
    package_dir={"": "src"},
    py_modules=["easypack"], 
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'Natural Language :: Russian',
        'Natural Language :: English',
        'License :: Creative Commons',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Framework :: IPython',
        'Framework :: M3',
        'Topic :: Software Development',
        'Topic :: Utilities',
    ],
)
